document.querySelector('.search').addEventListener("keyup", function () {
	if (this.value) {
		var search_url = "https://api.themoviedb.org/3/search/movie?api_key=1cbb61a21452dee385e8d65d32826843&language=en-US&query=" + this.value + "&page=1&include_adult=false";
		$.ajax({
			url: search_url,
			success: function (data) {
				render(data.results);
			}
		});
	} else {
		render(null);
	}
});

function render(data) {
	$('.results-container > div').remove();
	if (data != null) {
		var counter = 0;
		for (var i in data) {
			if (counter == 0) {
				var row = document.createElement('div');
				row.classList.add("movie-row");
				document.querySelector('.results-container').appendChild(row);
			}

			var movie = document.createElement('div');
			movie.classList.add('movie');

			var img = document.createElement('img');

			if (data[i].poster_path != null) {
				img.src = "https://image.tmdb.org/t/p/w500/" + data[i].poster_path;
			} else {
				img.src = "https://s3-ap-southeast-1.amazonaws.com/popcornsg/placeholder-movieimage.png";
			}

			var p = document.createElement('p');
			p.innerText = data[i].original_title;

			movie.appendChild(img);
			movie.appendChild(p);

			document.querySelector(".results-container > div:last-of-type").appendChild(movie);
			counter++;
			if (counter == 3) counter = 0;

			if (i == data.length - 1) {
				if ($('.results-container > div:last-of-type > div').length == 2) {
					$('.results-container > div:last-of-type').append("<div class='movie'></div>")
				} else if ($('.results-container > div:last-of-type > div').length == 1) {
					$('.results-container > div:last-of-type').append("<div class='movie'></div><div class='movie'></div>")
				}
			}
		}
	}
}
